const std = @import("std");

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const lib = b.addStaticLibrary(.{
        .name = "uuid",
        .target = target,
        .optimize = optimize,
    });
    lib.force_pic = true;
    lib.linkLibC();
    lib.addIncludePath(.{.path = "include"});
    lib.addCSourceFiles(.{
        .dependency = null,
        .files = &.{
	        "libuuid/src/clear.c",
	        "libuuid/src/compare.c",
	        "libuuid/src/copy.c",
	        "libuuid/src/gen_uuid.c",
	        "libuuid/src/isnull.c",
	        "libuuid/src/pack.c",
	        "libuuid/src/parse.c",
	        "libuuid/src/unpack.c",
	        "libuuid/src/unparse.c",
	        "libuuid/src/uuid_time.c",
	        "libuuid/src/predefined.c",
	        "lib/randutils.c",
	        "lib/md5.c",
	        "lib/sha1.c",
        },
        .flags = &.{
            "-D__linux__",
            "-DHAVE_UNISTD_H",
            "-DHAVE_STDLIB_H",
            "-DHAVE_SYS_TIME_H",
            "-DHAVE_SYS_FILE_H",
            "-DHAVE_SYS_IOCTL_H",
            "-DHAVE_SYS_SOCKET_H",
            "-DHAVE_SYS_UN_H",
            "-DHAVE_NET_IF_H",
            "-DHAVE_NETINET_IN_H",
            "-DHAVE_TLS",
            "-DSIOCGIFHWADDR",
            "-DHAVE_GETRANDOM",
            "-DHAVE_ERR_H",
            "-DHAVE_SYS_SYSMACROS_H",
            "-DHAVE_NANOSLEEP",
            "-DHAVE_USLEEP",
        },
    });
    b.installArtifact(lib);
}
