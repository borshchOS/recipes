const std = @import("std");

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

	const cflags = &[_][]const u8{
        "-Wall",
        "-Wextra",
        "-Werror",
        "-Wmissing-declarations",
        "-Wmissing-prototypes",
        "-Wcast-align",
        "-Wpointer-arith",
        "-Wreturn-type",
        "-DCONFIG_YAML",
        
        "-DPACKAGE_NAME=\"ifupdown-ng\"",
        "-DPACKAGE_BUGREPORT=\"https://github.com/ifupdown-ng/ifupdown-ng/issues/new\"",
        "-DPACKAGE_VERSION=\"0.12.1\"",
        "-DEXECUTOR_PATH=\"/pkg/ifupdown-ng/libexec\"",
        "-DINTERFACES_FILE=\"/etc/network/interfaces\"",
        "-DSTATE_FILE=\"/run/ifstate\"",
        "-DCONFIG_FILE=\"/etc/network/ifupdown-ng.conf\"",
    };
    
    const exe = b.addExecutable(.{
        .name = "ifupdown",
        .target = target,
        .optimize = optimize,
    });
    exe.addIncludePath(.{.path = "."});
    exe.linkLibC();
    exe.addCSourceFiles(.{
        .files = &.{
            "libifupdown/list.c",
            "libifupdown/dict.c",
            "libifupdown/interface.c",
            "libifupdown/interface-file.c",
            "libifupdown/fgetline.c",
            "libifupdown/version.c",
            "libifupdown/state.c",
            "libifupdown/environment.c",
            "libifupdown/execute.c",
            "libifupdown/lifecycle.c",
            "libifupdown/config-parser.c",
            "libifupdown/config-file.c",
            "libifupdown/compat.c",
            "libifupdown/yaml-base.c",
	        "libifupdown/yaml-writer.c",
	        
            "cmd/multicall.c",
	        "cmd/multicall-options.c",
	        "cmd/multicall-exec-options.c",
	        "cmd/multicall-match-options.c",
	        "cmd/pretty-print-iface.c",
	        "cmd/ifupdown.c",
	        "cmd/ifquery.c",
	        "cmd/ifctrstat.c",
	        "cmd/ifctrstat-linux.c",
	        "cmd/ifparse.c",
        },
        .flags = cflags,
    });
    
    b.installArtifact(exe);
}
