#!/bin/sh -a

# Build cache is referenced by $1 and defaults to $HOME/.cache/kiss/proc/<num>/pkg/<name>
DESTDIR=$1
PREFIX=pkg/$(basename $1) # mimicOS packages install prefix

# Use this to define pkg names and dirs, when built with different libc (e.g. "-musl", "-gnu")
SUFFIX="-gnu"
PKG_CONFIG_PATH=$KISS_ROOT/lib/pkgconfig$SUFFIX

# Generate .pc file
name="cmake"
version="3.29.3"
description="Cross-platform, open-source make system."
url="https://www.cmake.org/"
license="BSD-3-Clause"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license" > $name.pc

# Install and link build artifacts
install -d -m 755 \
    $1/bin \
    $1/lib/man/man1 \
    $1/lib/man/man7 \
    $1/$PREFIX/lib/pkgconfig$SUFFIX \
    $1/lib/pkgconfig$SUFFIX

mv bin $1/$PREFIX/
mv share $1/$PREFIX/
mv man $1/$PREFIX/lib/

mv $name.pc $1/$PREFIX/lib/pkgconfig$SUFFIX/

for f in $(find $1/$PREFIX/bin/* -type f && find $1/$PREFIX/bin/* -type l); do
    ln -sf ../$PREFIX/bin/$(basename $f) $1/bin/$(basename $f); done

for f in $(find $1/$PREFIX/lib/man/man1/* -type f && find $1/$PREFIX/lib/man/man1/* -type l); do
    ln -sf ../../../$PREFIX/lib/man/man1/$(basename $f) $1/lib/man/man1/$(basename $f); done

for f in $(find $1/$PREFIX/lib/man/man7/* -type f && find $1/$PREFIX/lib/man/man7/* -type l); do
    ln -sf ../../../$PREFIX/lib/man/man7/$(basename $f) $1/lib/man/man7/$(basename $f); done

for f in $(find $1/$PREFIX/lib/pkgconfig$SUFFIX/* -type f && find $1/$PREFIX/lib/pkgconfig$SUFFIX/* -type l); do
    ln -sf ../../$PREFIX/lib/pkgconfig$SUFFIX/$(basename $f) $1/lib/pkgconfig$SUFFIX/$(basename $f); done
