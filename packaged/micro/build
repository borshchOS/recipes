#!/bin/sh -a

# Build cache is referenced by $1 and defaults to $HOME/.cache/kiss/proc/<num>/pkg/<name>
DESTDIR=$1
PREFIX=pkg/$(basename $1) # mimicOS packages install prefix

# Use this to define pkg names and dirs, when built with different libc (e.g. "-musl", "-gnu")
SUFFIX=""
PKG_CONFIG_PATH=$KISS_ROOT/lib/pkgconfig$SUFFIX

# Generate .pc file
name="micro"
version="2.0.13"
description="A modern and intuitive terminal-based text editor."
url="https://micro-editor.github.io/"
license="MIT"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license" > $name.pc

# Install and link build artifacts
install -d -m 755 \
    $1/$PREFIX/bin \
    $1/$PREFIX/lib/applications \
    $1/$PREFIX/lib/icons/hicolor/scalable/apps \
    $1/$PREFIX/lib/man/man1 \
    $1/bin \
    $1/lib/applications\
    $1/lib/icons/hicolor/scalable/apps \
    $1/lib/man/man1 \
    $1/$PREFIX/lib/pkgconfig$SUFFIX \
    $1/lib/pkgconfig$SUFFIX \

install -m 755 micro $1/$PREFIX/bin
install -m 644 micro.1 $1/$PREFIX/lib/man/man1
install -m 644 micro.desktop $1/$PREFIX/lib/applications
install -m 644 micro.svg $1/$PREFIX/lib/icons/hicolor/scalable/apps

mv $name.pc $1/$PREFIX/lib/pkgconfig$SUFFIX/

ln -sf ../../$PREFIX/lib/applications/micro.desktop $1/lib/applications/micro.desktop

ln -sf ../../../../../$PREFIX/lib/icons/hicolor/scalable/apps/micro.svg $1/lib/icons/hicolor/scalable/apps/micro.svg

for f in $(find $1/$PREFIX/bin/* -type f && find $1/$PREFIX/bin/* -type l); do
    ln -sf ../$PREFIX/bin/$(basename $f) $1/bin/$(basename $f); done

for f in $(find $1/$PREFIX/lib/man/man1/* -type f && find $1/$PREFIX/lib/man/man1/* -type l); do
    ln -sf ../../../$PREFIX/lib/man/man1/$(basename $f) $1/lib/man/man1/$(basename $f); done

for f in $(find $1/$PREFIX/lib/pkgconfig$SUFFIX/* -type f && find $1/$PREFIX/lib/pkgconfig$SUFFIX/* -type l); do
    ln -sf ../../$PREFIX/lib/pkgconfig$SUFFIX/$(basename $f) $1/lib/pkgconfig$SUFFIX/$(basename $f); done
