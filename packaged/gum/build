#!/idx/bin/sh -a

TARGET=${TARGET:-x86_64-linux-musl} # Zig build target
MCPU=${MCPU:-baseline} # Examples: `baseline`, `native`, `generic+v7a`, or `arm1176jzf_s`
PREFIX=${PREFIX:-pkg/$(basename ${1})} # Zig build artifact install prefix
KISS_ROOT=${KISS_ROOT:-/} # Package install prefix
DESTDIR=${DESTDIR:-${1}} # Build cache. ${1} defaults to $HOME/.cache/kiss/proc/<num>/pkg/<name>
CC="zig cc -fno-sanitize=all -s -target ${TARGET} -mcpu=${MCPU}"
CXX="zig c++ -fno-sanitize=all -s -target ${TARGET} -mcpu=${MCPU}"
CFLAGS="-fPIC"
CXXFLAGS="-fPIC"
CMAKE_C_COMPILER="zig;cc;-fno-sanitize=all;-s;-target;${TARGET};-mcpu=${MCPU}"
CMAKE_CXX_COMPILER="zig;c++;-fno-sanitize=all;-s;-target;${TARGET};-mcpu=${MCPU}"
CMAKE_ASM_COMPILER=${CMAKE_C_COMPILER}
PKG_CONFIG_PATH="${KISS_ROOT}/idx/pcf"

# Generate .pc file
name="gum"
version="0.11.0"
description="Highly configurable utilities for writing interactive shell scripts."
url="https://github.com/charmbracelet/gum"
license="MIT"
requires=""

echo "Name: ${name}
Version: ${version}
Description: ${description}
URL: ${url}
License: ${license}
Requires: ${requires}" > ${name}.pc

# Install and link build artifacts
install -d -m 755 \
    ${DESTDIR}/${PREFIX}/bin \
    ${DESTDIR}/${PREFIX}/man/man1 \
    ${DESTDIR}/${PREFIX}/pcf \
    ${DESTDIR}/idx/bin \
    ${DESTDIR}/idx/man/man1 \
    ${DESTDIR}/idx/pcf

cp -a *.pc ${DESTDIR}/${PREFIX}/pcf/

install -m 755 gum ${DESTDIR}/${PREFIX}/bin
install -m 644 gum.1.gz ${DESTDIR}/${PREFIX}/man/man1

for f in $(find ${DESTDIR}/${PREFIX}/bin/* -type fl); do
    ln -sf ../../${PREFIX}/bin/$(basename ${f}) ${DESTDIR}/idx/bin/$(basename ${f});done

for f in $(find ${DESTDIR}/${PREFIX}/man/man1/* -type fl); do
    ln -sf ../../../${PREFIX}/man/man1/$(basename ${f}) ${DESTDIR}/idx/man/man1/$(basename ${f});done
    
for f in $(find ${DESTDIR}/${PREFIX}/pcf/* -type fl); do
    ln -sf ../../${PREFIX}/pcf/$(basename ${f}) ${DESTDIR}/idx/pcf/$(basename ${f});done
    
