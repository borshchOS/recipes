#!/bin/sh -a

# Build cache is referenced by $1 and defaults to $HOME/.cache/kiss/proc/<num>/pkg/<name>
DESTDIR=$1
PREFIX=pkg/$(basename $1) # mimicOS packages install prefix

# Use this to define pkg names and dirs, when built with different libc (e.g. "-musl", "-gnu")
SUFFIX="-musl"
PKG_CONFIG_PATH=$KISS_ROOT/lib/pkgconfig$SUFFIX

TARGET=${TARGET:-x86_64-linux-musl} # Zig build target
MCPU=${MCPU:-baseline} # Examples: `baseline`, `native`, `generic+v7a`, or `arm1176jzf_s`

CC="clang"
CFLAGS+="-nostdlibinc "
CFLAGS+="-fPIC "
CFLAGS+="-I$KISS_ROOT/pkg/musl/include "
LDFLAGS+="-L$KISS_ROOT/pkg/musl/lib "
LDFLAGS+="-L$KISS_ROOT/pkg/llvm-toolchain/lib "

./configure \
    --host=$TARGET \
    --build=$TARGET \
    --prefix=/$PREFIX \
    --sbindir=/$PREFIX/bin \
    --libdir=/$PREFIX/lib \
    --mandir=/$PREFIX/lib/man \
    --sysconfdir=/etc \
    --sharedstatedir=/var \
    --localstatedir=/var \
    --runstatedir=/run \
    --enable-shared \
    --disable-static \
    --without-examples \
    --without-tests \
    --without-docbook \
    --without-xmlwf \
    > /dev/null 2>&1

make > /dev/null 2>&1
make install > /dev/null 2>&1
make clean > /dev/null 2>&1

LDFLAGS+="--static "
./configure \
    --host=$TARGET \
    --build=$TARGET \
    --prefix=/$PREFIX \
    --sbindir=/$PREFIX/bin \
    --libdir=/$PREFIX/lib \
    --mandir=/$PREFIX/lib/man \
    --sysconfdir=/etc \
    --sharedstatedir=/var \
    --localstatedir=/var \
    --runstatedir=/run \
    --without-examples \
    --disable-shared \
    --without-tests \
    --without-docbook \
    > /dev/null 2>&1

make > /dev/null 2>&1
make install > /dev/null 2>&1

# Generate .pc file
name="expat"
version="2.6.2"
description="expat XML parser"
url="https://libexpat.github.io/"
license="MIT"
requires=""
libs="-L/pkg/$(basename $1)/lib -lexpat"
cflags="-I/pkg/$(basename $1)/include"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires
Libs: $libs
Cflags: $cflags" > $name.pc

# Install and link build artifacts
install -d -m 755 \
    $1/bin \
    $1/lib/man/man1 \
    $1/$PREFIX/lib/pkgconfig$SUFFIX \
    $1/lib/pkgconfig$SUFFIX

rm -rf $1/$PREFIX/lib/pkgconfig
mv $name.pc $1/$PREFIX/lib/pkgconfig$SUFFIX/

for f in $(find $1/$PREFIX/bin/* -type f && find $1/$PREFIX/bin/* -type l); do
    ln -sf ../$PREFIX/bin/$(basename $f) $1/bin/$(basename $f); done

for f in $(find $1/$PREFIX/lib/man/man1/* -type f && find $1/$PREFIX/lib/man/man1/* -type l); do
    ln -sf ../../../$PREFIX/lib/man/man1/$(basename $f) $1/lib/man/man1/$(basename $f); done

for f in $(find $1/$PREFIX/lib/pkgconfig$SUFFIX/* -type f && find $1/$PREFIX/lib/pkgconfig$SUFFIX/* -type l); do
    ln -sf ../../$PREFIX/lib/pkgconfig$SUFFIX/$(basename $f) $1/lib/pkgconfig$SUFFIX/$(basename $f); done
