#!/bin/sh

if [ ! -e "$XDG_RUNTIME_DIR/bus" ]; then
    # create new session bus
    export $(dbus-launch)
    echo "${DBUS_SESSION_BUS_ADDRESS}" > "$XDG_RUNTIME_DIR/bus"
    echo "${DBUS_SESSION_BUS_PID}" >> "$XDG_RUNTIME_DIR/bus"
else
    # reuse existing session bus for the same user
    export DBUS_SESSION_BUS_ADDRESS="$(head -n 1 $XDG_RUNTIME_DIR/bus)"
    export DBUS_SESSION_BUS_PID="$(tail -1 $XDG_RUNTIME_DIR/bus)"
fi
