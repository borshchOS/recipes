const std = @import("std");

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const exe = b.addExecutable(.{
        .name = "doas",
        .target = target,
        .optimize = optimize,
    });
    exe.addIncludePath(.{.path = "."});
    exe.addIncludePath(.{.path = "libopenbsd"});
    exe.linkLibC();
    exe.addCSourceFiles(.{
        .files = &.{
            "doas.c",
            "env.c",
            "parse.c",
            "timestamp.c",
            "libopenbsd/errc.c",
            "libopenbsd/verrc.c",
            "libopenbsd/progname.c",
            "libopenbsd/readpassphrase.c",
            "libopenbsd/strtonum.c",
            "libopenbsd/bsd-setres_id.c",
            "libopenbsd/closefrom.c",
            "shadow.c",
        },
        .flags = &.{
            "-Wall",
            "-Wextra",
            "-D__musl__",
		    "-static",
		    "-D_DEFAULT_SOURCE",
		    "-D_GNU_SOURCE",
		    "-DUID_MAX=65535",
		    "-DGID_MAX=65535",
        },
    });
    
    b.installArtifact(exe);
}
