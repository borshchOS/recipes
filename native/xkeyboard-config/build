#!/bin/sh -a

# Build cache is referenced by $1 and defaults to $HOME/.cache/kiss/proc/<num>/pkg/<name>
DESTDIR=$1
PREFIX=pkg/$(basename $1) # mimicOS packages install prefix
TARGET=${TARGET:-x86_64-linux-musl} # Zig build target
CC="zig cc -fno-sanitize=all -target $TARGET"
LD="zig cc -fno-sanitize=all -s -target $TARGET -mcpu=$MCPU"

python -m mesonbuild.mesonmain setup build/ \
    --prefix=/$PREFIX \
    --libdir=lib \
    -Dbuildtype=minsize \
    -Ddefault_library=static \
    -Dprefer_static=true \
    -Db_staticpic=true \
    -Db_pie=false \
    > /dev/null 2>&1
    
ninja -C build/ install > /dev/null 2>&1

# Generate .pc file
name="xkeyboard-config"
version="2.41"
description="X keyboard configuration files"
url="https://gitlab.freedesktop.org/xkeyboard-config/xkeyboard-config"
license="MIT"
xkb_base="/share/X11/xkb"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
xkb_base=$xkb_base" > $name.pc

# Install and link build artifacts
install -d -m 755 \
    $1/$PREFIX/share/pkgconfig \
    $1/share/X11 \
    $1/share/man/man7 \
    $1/share/pkgconfig

mv $name.pc $1/$PREFIX/share/pkgconfig/

# (option) Install modern Ukrainian layouts
sed -i.orig '/ukr/,/crh/s/phonetic/qwertyvka/g' $1/$PREFIX/share/X11/xkb/rules/evdev.xml
sed -i.orig '/ukr/,/crh/s/typewriter/colemakivka/g' $1/$PREFIX/share/X11/xkb/rules/evdev.xml
sed -i.orig '/ukr/,/crh/s/legacy/workmanivka/g' $1/$PREFIX/share/X11/xkb/rules/evdev.xml
sed -i.orig '/ukr/,/crh/s/homophonic/dvorakivka/g' $1/$PREFIX/share/X11/xkb/rules/evdev.xml
rm $1/$PREFIX/share/X11/xkb/rules/evdev.xml.orig
rm $1/$PREFIX/share/X11/xkb/rules/base.xml
ln -sf evdev.xml $1/$PREFIX/share/X11/xkb/rules/base.xml
cat unix/ua >> $1/$PREFIX/share/X11/xkb/symbols/ua
# option code ends here

ln -sf ../../$PREFIX/share/X11/xkb $1/share/X11/xkb
    
for f in $(find $1/$PREFIX/share/man/man7/* -type fl); do
    ln -sf ../../../$PREFIX/share/man/man7/$(basename $f) $1/share/man/man7/$(basename $f); done
    
for f in $(find $1/$PREFIX/share/pkgconfig/* -type fl); do
    ln -sf ../../$PREFIX/share/pkgconfig/$(basename $f) $1/share/pkgconfig/$(basename $f); done
