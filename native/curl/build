#!/bin/sh -a

# Build cache is referenced by $1 and defaults to $HOME/.cache/kiss/proc/<num>/pkg/<name>
DESTDIR=$1
PREFIX=pkg/$(basename $1) # mimicOS packages install prefix
TARGET=${TARGET:-x86_64-linux-musl} # Zig build target
MCPU=${MCPU:-baseline} # Examples: `baseline`, `native`, `generic+v7a`, or `arm1176jzf_s`
CC=clang
CFLAGS="-fPIC"
LD=$CC

PATH=$KISS_ROOT/bin \
PKG_CONFIG_PATH=$KISS_ROOT/share/pkgconfig \
./configure \
    --host=$TARGET \
    --build=$TARGET \
    --prefix=/$PREFIX \
    --sbindir=/$PREFIX/bin \
    --libdir=/$PREFIX/lib \
    --sysconfdir=/etc \
    --sharedstatedir=/var \
    --localstatedir=/var \
    --runstatedir=/run \
    --disable-shared \
    --enable-static \
    --enable-websockets \
    --enable-ipv6 \
    --enable-manual \
    --with-zstd \
    --with-openssl=/pkg/libressl \
    --with-ca-bundle=/etc/libressl/cert.pem \
    --with-ca-path=/etc/libressl/certs \
    --with-nghttp2 \
    --with-libssh2 \
    > /dev/null 2>&1

make install > /dev/null 2>&1

# Generate .pc file
name="libcurl"
version="8.7.1"
description="URL retrival library"
url="https://curl.se/"
license="curl"
requires="libcrypto-libre,libssl-libre,zlib,libzstd,libssh2,libnghttp2"
libs="-L\${pcfiledir}/../../pkg/$(basename $1)/lib -lcurl"
cflags="-I\${pcfiledir}/../../pkg/$(basename $1)/include -DCURL_STATICLIB"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires
Libs: $libs
Cflags: $cflags" > $name.pc

# Install and link build artifacts
install -d -m 755 \
    $1/$PREFIX/share/pkgconfig \
    $1/bin \
    $1/share/man/man1 \
    $1/share/man/man3 \
    $1/share/pkgconfig

rm -rf $1/$PREFIX/lib/pkgconfig
mv $name.pc $1/$PREFIX/share/pkgconfig/

for f in $(find $1/$PREFIX/bin/* -type fl); do
    ln -sf ../$PREFIX/bin/$(basename $f) $1/bin/$(basename $f); done

for f in $(find $1/$PREFIX/share/man/man1/* -type fl); do
    ln -sf ../../../$PREFIX/share/man/man1/$(basename $f) $1/share/man/man1/$(basename $f); done

for f in $(find $1/$PREFIX/share/man/man3/* -type fl); do
    ln -sf ../../../$PREFIX/share/man/man3/$(basename $f) $1/share/man/man3/$(basename $f); done

for f in $(find $1/$PREFIX/share/pkgconfig/* -type fl); do
    ln -sf ../../$PREFIX/share/pkgconfig/$(basename $f) $1/share/pkgconfig/$(basename $f); done
