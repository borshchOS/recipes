#!/bin/sh -a

# Build cache is referenced by $1 and defaults to $HOME/.cache/kiss/proc/<num>/pkg/<name>
DESTDIR=$1
PREFIX=pkg/$(basename $1) # mimicOS packages install prefix
TARGET=${TARGET:-x86_64-linux-musl} # Zig build target
MCPU=${MCPU:-baseline} # Examples: `baseline`, `native`, `generic+v7a`, or `arm1176jzf_s`
OPTIMIZE=${OPTIMIZE:-ReleaseSmall} # Zig build options: Debug, ReleaseSafe, ReleaseFast, ReleaseSmall
CC="zig cc -fno-sanitize=all -s -target $TARGET -mcpu=$MCPU"
LD=$CC

gmake \
    HOST_SH=/bin/sh \
    BINDIR=/$PREFIX/bin \
    LIBDIR=/$PREFIX/lib \
    INCDIR=/$PREFIX/include \
    LDFLAGS="-static" \
    all-static install-headers install-tic install-stalibs > /dev/null 2>&1

# Generate .pc file
name="curses-netbsd"
version="5.9"
description="curses terminal library."
url="https://github.com/sabotage-linux/netbsd-curses"
license="BSD-3-Clause AND BSD-2-Clause"
requires=""
libs="-L\${pcfiledir}/../../pkg/$(basename $1)/lib -lcurses -lterminfo"
cflags="-I\${pcfiledir}/../../pkg/$(basename $1)/include"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires
Libs: $libs
Cflags: $cflags" > $name.pc

name="form-netbsd"
version="5.9"
description="curses form library."
url="https://github.com/sabotage-linux/netbsd-curses"
license="BSD-3-Clause AND BSD-2-Clause"
requires="curses-netbsd"
libs="-L\${pcfiledir}/../../pkg/$(basename $1)/lib -lform"
cflags="-I\${pcfiledir}/../../pkg/$(basename $1)/include"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires
Libs: $libs
Cflags: $cflags" > $name.pc

name="menu-netbsd"
version="5.9"
description="curses menu library."
url="https://github.com/sabotage-linux/netbsd-curses"
license="BSD-3-Clause AND BSD-2-Clause"
requires="curses-netbsd"
libs="-L\${pcfiledir}/../../pkg/$(basename $1)/lib -lmenu"
cflags="-I\${pcfiledir}/../../pkg/$(basename $1)/include"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires
Libs: $libs
Cflags: $cflags" > $name.pc

name="panel-netbsd"
version="5.9"
description="curses panel library."
url="https://github.com/sabotage-linux/netbsd-curses"
license="BSD-3-Clause AND BSD-2-Clause"
requires="curses-netbsd"
libs="-L\${pcfiledir}/../../pkg/$(basename $1)/lib -lpanel"
cflags="-I\${pcfiledir}/../../pkg/$(basename $1)/include"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires
Libs: $libs
Cflags: $cflags" > $name.pc

name="terminfo-netbsd"
version="5.9"
description="terminfo library."
url="https://github.com/sabotage-linux/netbsd-curses"
license="BSD-3-Clause AND BSD-2-Clause"
requires=""
libs="-L\${pcfiledir}/../../pkg/$(basename $1)/lib -lterminfo"
cflags="-I\${pcfiledir}/../../pkg/$(basename $1)/include"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires
Libs: $libs
Cflags: $cflags" > $name.pc
          
# Install and link build artifacts
install -d -m 755 \
    $1/$PREFIX/share \
    $1/$PREFIX/share/pkgconfig \
    $1/bin \
    $1/share/terminfo \
    $1/share/pkgconfig

ln -sf curses-netbsd.pc ncurses-netbsd.pc
ln -sf curses-netbsd.pc ncursesw-netbsd.pc
ln -sf form-netbsd.pc formw-netbsd.pc
ln -sf menu-netbsd.pc menuw-netbsd.pc
ln -sf panel-netbsd.pc panelw-netbsd.pc
ln -sf terminfo-netbsd.pc termcap-netbsd.pc

cp -a *.pc $1/$PREFIX/share/pkgconfig/

mv terminfo $1/$PREFIX/share/

for f in $(find $1/$PREFIX/bin/* -type fl); do
    ln -sf ../$PREFIX/bin/$(basename $f) $1/bin/$(basename $f); done

for f in $(find $1/$PREFIX/share/terminfo/* -type fl); do
    ln -sf ../../$PREFIX/share/terminfo/$(basename $f) $1/share/terminfo/$(basename $f); done
    
for f in $(find $1/$PREFIX/share/pkgconfig/* -type fl); do
    ln -sf ../../$PREFIX/share/pkgconfig/$(basename $f) $1/share/pkgconfig/$(basename $f); done
