#!/bin/sh -a

# Build cache is referenced by $1 and defaults to $HOME/.cache/kiss/proc/<num>/pkg/<name>
DESTDIR=$1
PREFIX=pkg/$(basename $1) # mimicOS packages install prefix
TARGET=${TARGET:-x86_64-linux-musl} # Zig build target
CC="zig cc -fno-sanitize=all -target $TARGET"
LD="zig cc -fno-sanitize=all -s -target $TARGET -mcpu=$MCPU"

PKG_CONFIG_PATH=$KISS_ROOT/share/pkgconfig \
python -m mesonbuild.mesonmain setup build/ \
    --prefix=/$PREFIX \
    -Dbuildtype=minsize \
    -Ddefault_library=static \
    -Dprefer_static=true \
    -Db_staticpic=true \
    -Db_pie=false \
    > /dev/null 2>&1
    
ninja -C build/ install > /dev/null 2>&1

# Generate .pc file
name="wayland-utils"
version="1.2.0"
description="Wayland utils tools."
url="https://gitlab.freedesktop.org/wayland/wayland-utils"
license="MIT"
requires="libdrm,wayland-client,wayland-scanner,wayland-protocols"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires" > $name.pc

# Install and link build artifacts
install -d -m 755 \
    $1/$PREFIX/share/pkgconfig \
    $1/bin \
    $1/share/man/man1 \
    $1/share/pkgconfig

cp -a *.pc $1/$PREFIX/share/pkgconfig/

for f in $(find $1/$PREFIX/bin/* -type fl); do
    ln -sf ../$PREFIX/bin/$(basename $f) $1/bin/$(basename $f); done

for f in $(find $1/$PREFIX/share/man/man1/* -type fl); do
    ln -sf ../../../$PREFIX/share/man/man1/$(basename $f) $1/share/man/man1/$(basename $f); done
    
for f in $(find $1/$PREFIX/share/pkgconfig/* -type fl); do
    ln -sf ../../$PREFIX/share/pkgconfig/$(basename $f) $1/share/pkgconfig/$(basename $f); done
