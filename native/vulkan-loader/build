#!/bin/sh -a

# Build cache is referenced by $1 and defaults to $HOME/.cache/kiss/proc/<num>/pkg/<name>
DESTDIR=$1
PREFIX=pkg/$(basename $1) # mimicOS packages install prefix
TARGET=${TARGET:-x86_64-linux-musl} # Zig build target
MCPU=${MCPU:-baseline} # Examples: `baseline`, `native`, `generic+v7a`, or `arm1176jzf_s`
CC="zig cc -fno-sanitize=all -s -target $TARGET -mcpu=$MCPU"
CMAKE_C_COMPILER="zig;cc;-fno-sanitize=all;-s;-target;$TARGET;-mcpu=$MCPU"
CFLAGS="-fPIC"

cmake -B build \
  -Wno-dev \
  -DCMAKE_WARN_DEPRECATED=False \
  -DCMAKE_GENERATOR=Ninja \
  -DCMAKE_BUILD_TYPE=MinSizeRel \
  -DCMAKE_INSTALL_PREFIX=/pkg/vulkan-loader \
  -DCMAKE_INSTALL_LIBDIR=lib \
  -DCMAKE_C_COMPILER=$CMAKE_C_COMPILER \
  -DCMAKE_SKIP_RPATH=YES \
  \
  -DUSE_GAS=OFF \
  -DAPPLE_STATIC_LOADER=OFF \
  -DBUILD_WSI_XCB_SUPPORT=OFF \
  -DBUILD_WSI_XLIB_SUPPORT=OFF \
  -DBUILD_WSI_WAYLAND_SUPPORT=ON \
  -DLOADER_CODEGEN=OFF \
  -DVulkanHeaders_DIR="$KISS_ROOT/pkg/vulkan-headers/share/cmake/VulkanHeaders" \
  -DVULKAN_HEADERS_INSTALL_DIR="$KISS_ROOT/pkg/vulkan-headers/include" \
  > /dev/null 2>&1 

cmake --build   build > /dev/null 2>&1
cmake --install build > /dev/null 2>&1

# Generate .pc file
name="vulkan"
version="1.3.283"
description="Vulkan Installable Client Driver (ICD) Loader"
url="https://www.khronos.org/vulkan"
license="Apache-2.0"
requires="vulkan-headers"
libs="-L\${pcfiledir}/../../pkg/$(basename $1)/lib -lvulkan"
cflags="-I\${pcfiledir}/../../pkg/$(basename $1)/include"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires
Libs: $libs
Cflags: $cflags" > $name.pc

# Install and link build artifacts
install -d -m 755 \
	$1/$PREFIX/lib \
    $1/$PREFIX/share/pkgconfig \
    $1/share/pkgconfig

cp -a *.pc $1/$PREFIX/share/pkgconfig/

rm -rf $1/$PREFIX/lib/pkgconfig
    
for f in $(find $1/$PREFIX/share/pkgconfig/* -type fl); do
    ln -sf ../../$PREFIX/share/pkgconfig/$(basename $f) $1/share/pkgconfig/$(basename $f); done
 

