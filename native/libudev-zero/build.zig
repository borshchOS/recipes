const std = @import("std");

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const lib = b.addStaticLibrary(.{
        .name = "udev",
        .target = target,
        .optimize = optimize,
        .pic = true,
    });
    lib.linkLibC();
    lib.addCSourceFiles(.{
        .files = &.{
            "udev.c",
            "udev_list.c",
            "udev_device.c",
            "udev_monitor.c",
            "udev_enumerate.c",
        },
        .flags = &.{
            "-D_XOPEN_SOURCE=700",
            "-Wall",
            "-std=c99",
            "-Wextra",
            "-Wpedantic",
		    "-Wmissing-prototypes",
		    "-Wstrict-prototypes",
		    "-Wno-unused-parameter",
		    "-pthread",
        },
    });
    
    b.installArtifact(lib);
}
