#!/bin/sh -a

# Build cache is referenced by $1 and defaults to $HOME/.cache/kiss/proc/<num>/pkg/<name>
DESTDIR=$1
PREFIX=pkg/$(basename $1) # mimicOS packages install prefix
TARGET=${TARGET:-x86_64-linux-musl} # Zig build target
CC=clang
PATH=$KISS_ROOT/bin
PKG_CONFIG_PATH=$KISS_ROOT/share/pkgconfig
CFLAGS="-fPIC -static"
LDFLAGS="-static"

# fix static linking for tests
sed -i.orig 's:shared_lib:static_lib:g' subprojects/libcshquote-1/src/meson.build
sed -i.orig 's:shared_lib:static_lib:g' subprojects/libcrbtree-3/src/meson.build
sed -i.orig 's:shared_lib:static_lib:g' subprojects/libcutf8-1/src/meson.build
sed -i.orig 's:shared_lib:static_lib:g' subprojects/libcini-1/src/meson.build
sed -i.orig 's:shared_lib:static_lib:g' subprojects/libcdvar-1/src/meson.build

python -m mesonbuild.mesonmain setup build/ \
    --prefix=/$PREFIX \
    --libdir=lib \
    --sysconfdir=/etc \
    --localstatedir=/var \
    -Dbuildtype=minsize \
    -Ddefault_library=static \
    -Dprefer_static=true \
    -Db_staticpic=true \
    -Db_pie=false \
	\
	-Dlauncher=false \
	-Ddocs=false \
    > /dev/null 2>&1

ninja -C build/ install > /dev/null 2>&1
  
# Generate .pc file
name="dbus-broker"
version="36"
description="Linux D-Bus Message Broker"
url="https://github.com/bus1/dbus-broker"
license="Apache-2.0"
requires=""
libs=""
cflags=""

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires
Libs: $libs
Cflags: $cflags" > $name.pc

# Install and link build artifacts
install -d -m 755 \
    $1/$PREFIX/share/pkgconfig \
    $1/bin \
    $1/share/pkgconfig

cp -a *.pc $1/$PREFIX/share/pkgconfig/

rm -rf $1/run
rm -rf $1/$PREFIX/lib/pkgconfig

for f in $(find $1/$PREFIX/bin/* -type fl); do
    ln -sf ../$PREFIX/bin/$(basename $f) $1/bin/$(basename $f); done

for f in $(find $1/$PREFIX/share/pkgconfig/* -type fl); do
    ln -sf ../../$PREFIX/share/pkgconfig/$(basename $f) $1/share/pkgconfig/$(basename $f); done
