const std = @import("std");

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

	const cflags = &[_][]const u8{
        "-DHAVE_CONFIG_H",
        "-D_GNU_SOURCE",
    };

    const lib = b.addStaticLibrary(.{
        .name = "shadow",
        .target = target,
        .optimize = optimize,
        .pic = true,
    });
    lib.addSystemIncludePath(.{.path = "."});
    lib.addSystemIncludePath(.{.path = "lib"});
    lib.addCSourceFiles(.{
        .files = &.{
            "lib/addgrps.c",
            "lib/adds.c",
            "lib/age.c",
            "lib/agetpass.c",
            "lib/alloc.c",
            "lib/atoi/strtoi.c",
            "lib/atoi/strtou_noneg.c",
            "lib/audit_help.c",
            "lib/basename.c",
            "lib/bit.c",
            "lib/chkname.c",
            "lib/chowndir.c",
            "lib/chowntty.c",
            "lib/cleanup.c",
            "lib/cleanup_group.c",
            "lib/cleanup_user.c",
            "lib/commonio.c",
            "lib/console.c",
            "lib/copydir.c",
            "lib/csrand.c",
            "lib/encrypt.c",
            "lib/env.c",
            "lib/failure.c",
            "lib/fd.c",
            "lib/fields.c",
            "lib/find_new_gid.c",
            "lib/find_new_uid.c",
            "lib/find_new_sub_gids.c",
            "lib/find_new_sub_uids.c",
            "lib/fputsx.c",
            "lib/get_gid.c",
            "lib/get_pid.c",
            "lib/get_uid.c",
            "lib/getdate.c",
            "lib/getdef.c",
            "lib/getlong.c",
            "lib/getgr_nam_gid.c",
            "lib/getrange.c",
            "lib/gettime.c",
            "lib/getulong.c",
            "lib/groupio.c",
            "lib/groupmem.c",
            "lib/gshadow.c",
            "lib/hushed.c",
            "lib/idmapping.c",
            "lib/isexpired.c",
            "lib/limits.c",
            "lib/list.c",
            "lib/lockpw.c",
            "lib/loginprompt.c",
            "lib/mail.c",
            "lib/memzero.c",
            "lib/motd.c",
            "lib/myname.c",
            "lib/nss.c",
            "lib/nscd.c",
            "lib/obscure.c",
            "lib/pam_pass.c",
            "lib/pam_pass_non_interactive.c",
            "lib/port.c",
            "lib/prefix_flag.c",
            "lib/pwauth.c",
            "lib/pwio.c",
            "lib/pwd_init.c",
            "lib/pwd2spwd.c",
            "lib/pwdcheck.c",
            "lib/pwmem.c",
            "lib/remove_tree.c",
            "lib/rlogin.c",
            "lib/root_flag.c",
            "lib/run_part.c",
            "lib/salt.c",
            "lib/selinux.c",
            "lib/semanage.c",
            "lib/setugid.c",
            "lib/setupenv.c",
            "lib/sgetgrent.c",
            "lib/sgetpwent.c",
            "lib/sgetspent.c",
            "lib/sgroupio.c",
            "lib/shadow.c",
            "lib/shadowio.c",
            "lib/shadowlog.c",
            "lib/shadowmem.c",
            "lib/shell.c",
            "lib/spawn.c",
            "lib/sssd.c",
            "lib/string/sprintf.c",
            "lib/string/stpecpy.c",
            "lib/string/stpeprintf.c",
            "lib/string/strftime.c",
            "lib/string/strtcpy.c",
            "lib/strtoday.c",
            "lib/sub.c",
            "lib/subordinateio.c",
            "lib/sulog.c",
            "lib/time/day_to_str.c",
            "lib/ttytype.c",
            "lib/tz.c",
            "lib/ulimit.c",
            "lib/user_busy.c",
            "lib/valid.c",
            "lib/write_full.c",
            "lib/xgetpwnam.c",
            "lib/xprefix_getpwnam.c",
            "lib/xgetpwuid.c",
            "lib/xgetgrnam.c",
            "lib/xgetgrgid.c",
            "lib/xgetspnam.c",
            "lib/yesno.c",

            // compat due to not using libbsd
	        "lib/freezero.c",
	        "lib/readpassphrase.c",

            // not using logind
            "lib/utmp.c",
        },
        .flags = cflags,
    });
    lib.linkLibC();
	
    const exe01 = b.addExecutable(.{
        .name = "chage",
        .target = target,
        .optimize = optimize,
    });
    exe01.addIncludePath(.{.path = "lib"});
    exe01.addSystemIncludePath(.{.path = "."});
    exe01.addCSourceFiles(.{
        .files = &.{
            "src/chage.c",
        },
        .flags = cflags,
    });
    exe01.linkLibC();
    exe01.linkLibrary(lib);
    b.installArtifact(exe01);

    const exe02 = b.addExecutable(.{
        .name = "chfn",
        .target = target,
        .optimize = optimize,
    });
    exe02.addIncludePath(.{.path = "lib"});
    exe02.addSystemIncludePath(.{.path = "."});
    exe02.addCSourceFiles(.{
        .files = &.{
            "src/chfn.c",
        },
        .flags = cflags,
    });
    exe02.linkLibC();
    exe02.linkLibrary(lib);
    b.installArtifact(exe02);

    const exe03 = b.addExecutable(.{
        .name = "chsh",
        .target = target,
        .optimize = optimize,
    });
    exe03.addIncludePath(.{.path = "lib"});
    exe03.addSystemIncludePath(.{.path = "."});
    exe03.addCSourceFiles(.{
        .files = &.{
            "src/chsh.c",
        },
        .flags = cflags,
    });
    exe03.linkLibC();
    exe03.linkLibrary(lib);
    b.installArtifact(exe03);
 
    const exe04 = b.addExecutable(.{
        .name = "groups",
        .target = target,
        .optimize = optimize,
    });
    exe04.addIncludePath(.{.path = "lib"});
    exe04.addSystemIncludePath(.{.path = "."});
    exe04.addCSourceFiles(.{
        .files = &.{
            "src/groups.c",
        },
        .flags = cflags,
    });
    exe04.linkLibC();
    exe04.linkLibrary(lib);
    b.installArtifact(exe04);
    
    const exe05 = b.addExecutable(.{
        .name = "login",
        .target = target,
        .optimize = optimize,
      });
    exe05.addIncludePath(.{.path = "lib"});
    exe05.addSystemIncludePath(.{.path = "."});
    exe05.addCSourceFiles(.{
        .files = &.{
            "src/login.c",
            "src/login_nopam.c",
        },
        .flags = cflags,
    });
    exe05.linkLibC();
    exe05.linkLibrary(lib);
    b.installArtifact(exe05);
    
    const exe06 = b.addExecutable(.{
        .name = "nologin",
        .target = target,
        .optimize = optimize,
    });
    exe06.addIncludePath(.{.path = "lib"});
    exe06.addSystemIncludePath(.{.path = "."});
    exe06.addCSourceFiles(.{
        .files = &.{
            "src/nologin.c",
        },
        .flags = cflags,
    });
    exe06.linkLibC();
    exe06.linkLibrary(lib);
    b.installArtifact(exe06);
    
    const exe07 = b.addExecutable(.{
        .name = "faillog",
        .target = target,
        .optimize = optimize,
    });
    exe07.addIncludePath(.{.path = "lib"});
    exe07.addSystemIncludePath(.{.path = "."});
    exe07.addCSourceFiles(.{
        .files = &.{
            "src/faillog.c",
        },
        .flags = cflags,
    });
    exe07.linkLibC();
    exe07.linkLibrary(lib);
    b.installArtifact(exe07);

    const exe08 = b.addExecutable(.{
        .name = "expiry",
        .target = target,
        .optimize = optimize,
    });
    exe08.addIncludePath(.{.path = "lib"});
    exe08.addSystemIncludePath(.{.path = "."});
    exe08.addCSourceFiles(.{
        .files = &.{
            "src/expiry.c",
        },
        .flags = cflags,
    });
    exe08.linkLibC();
    exe08.linkLibrary(lib);
    b.installArtifact(exe08);

    const exe09 = b.addExecutable(.{
        .name = "gpasswd",
        .target = target,
        .optimize = optimize,
    });
    exe09.addIncludePath(.{.path = "lib"});
    exe09.addSystemIncludePath(.{.path = "."});
    exe09.addCSourceFiles(.{
        .files = &.{
            "src/gpasswd.c",
        },
        .flags = cflags,
    });
    exe09.linkLibC();
    exe09.linkLibrary(lib);
    b.installArtifact(exe09);

    const exe10 = b.addExecutable(.{
        .name = "newgrp",
        .target = target,
        .optimize = optimize,
    });
    exe10.addIncludePath(.{.path = "lib"});
    exe10.addSystemIncludePath(.{.path = "."});
    exe10.addCSourceFiles(.{
        .files = &.{
            "src/newgrp.c",
        },
        .flags = cflags,
    });
    exe10.linkLibC();
    exe10.linkLibrary(lib);
    b.installArtifact(exe10);

    const exe11 = b.addExecutable(.{
        .name = "passwd",
        .target = target,
        .optimize = optimize,
    });
    exe11.addIncludePath(.{.path = "lib"});
    exe11.addSystemIncludePath(.{.path = "."});
    exe11.addCSourceFiles(.{
        .files = &.{
            "src/passwd.c",
        },
        .flags = cflags,
    });
    exe11.linkLibC();
    exe11.linkLibrary(lib);
    b.installArtifact(exe11);

    const exe12 = b.addExecutable(.{
        .name = "chgpasswd",
        .target = target,
        .optimize = optimize,
    });
    exe12.addIncludePath(.{.path = "lib"});
    exe12.addSystemIncludePath(.{.path = "."});
    exe12.addCSourceFiles(.{
        .files = &.{
            "src/chgpasswd.c",
        },
        .flags = cflags,
    });
    exe12.linkLibC();
    exe12.linkLibrary(lib);
    b.installArtifact(exe12);
    
    
    const exe13 = b.addExecutable(.{
        .name = "chpasswd",
        .target = target,
        .optimize = optimize,
    });
    exe13.addIncludePath(.{.path = "lib"});
    exe13.addSystemIncludePath(.{.path = "."});
    exe13.addCSourceFiles(.{
        .files = &.{
            "src/chpasswd.c",
        },
        .flags = cflags,
    });
    exe13.linkLibC();
    exe13.linkLibrary(lib);
    b.installArtifact(exe13);
    
    const exe14 = b.addExecutable(.{
        .name = "groupadd",
        .target = target,
        .optimize = optimize,
    });
    exe14.addIncludePath(.{.path = "lib"});
    exe14.addSystemIncludePath(.{.path = "."});
    exe14.addCSourceFiles(.{
        .files = &.{
            "src/groupadd.c",
        },
        .flags = cflags,
    });
    exe14.linkLibC();
    exe14.linkLibrary(lib);
    b.installArtifact(exe14);
 
    const exe15 = b.addExecutable(.{
        .name = "groupdel",
        .target = target,
        .optimize = optimize,
    });
    exe15.addIncludePath(.{.path = "lib"});
    exe15.addSystemIncludePath(.{.path = "."});
    exe15.addCSourceFiles(.{
        .files = &.{
            "src/groupdel.c",
        },
        .flags = cflags,
    });
    exe15.linkLibC();
    exe15.linkLibrary(lib);
    b.installArtifact(exe15);

    const exe16 = b.addExecutable(.{
        .name = "groupmems",
        .target = target,
        .optimize = optimize,
    });
    exe16.addIncludePath(.{.path = "lib"});
    exe16.addSystemIncludePath(.{.path = "."});
    exe16.addCSourceFiles(.{
        .files = &.{
            "src/groupmems.c",
        },
        .flags = cflags,
    });
    exe16.linkLibC();
    exe16.linkLibrary(lib);
    b.installArtifact(exe16);

    const exe17 = b.addExecutable(.{
        .name = "groupmod",
        .target = target,
        .optimize = optimize,
    });
    exe17.addIncludePath(.{.path = "lib"});
    exe17.addSystemIncludePath(.{.path = "."});
    exe17.addCSourceFiles(.{
        .files = &.{
            "src/groupmod.c",
        },
        .flags = cflags,
    });
    exe17.linkLibC();
    exe17.linkLibrary(lib);
    b.installArtifact(exe17);

    const exe18 = b.addExecutable(.{
        .name = "grpck",
        .target = target,
        .optimize = optimize,
    });
    exe18.addIncludePath(.{.path = "lib"});
    exe18.addSystemIncludePath(.{.path = "."});
    exe18.addCSourceFiles(.{
        .files = &.{
            "src/grpck.c",
        },
        .flags = cflags,
    });
    exe18.linkLibC();
    exe18.linkLibrary(lib);
    b.installArtifact(exe18);

    const exe19 = b.addExecutable(.{
        .name = "grpconv",
        .target = target,
        .optimize = optimize,
    });
    exe19.addIncludePath(.{.path = "lib"});
    exe19.addSystemIncludePath(.{.path = "."});
    exe19.addCSourceFiles(.{
        .files = &.{
            "src/grpconv.c",
        },
        .flags = cflags,
    });
    exe19.linkLibC();
    exe19.linkLibrary(lib);
    b.installArtifact(exe19);

    const exe20 = b.addExecutable(.{
        .name = "grpunconv",
        .target = target,
        .optimize = optimize,
    });
    exe20.addIncludePath(.{.path = "lib"});
    exe20.addSystemIncludePath(.{.path = "."});
    exe20.addCSourceFiles(.{
        .files = &.{
            "src/grpunconv.c",
        },
        .flags = cflags,
    });
    exe20.linkLibC();
    exe20.linkLibrary(lib);
    b.installArtifact(exe20);

    const exe21 = b.addExecutable(.{
        .name = "logoutd",
        .target = target,
        .optimize = optimize,
    });
    exe21.addIncludePath(.{.path = "lib"});
    exe21.addSystemIncludePath(.{.path = "."});
    exe21.addCSourceFiles(.{
        .files = &.{
            "src/logoutd.c",
        },
        .flags = cflags,
    });
    exe21.linkLibC();
    exe21.linkLibrary(lib);
    b.installArtifact(exe21);

    const exe22 = b.addExecutable(.{
        .name = "newusers",
        .target = target,
        .optimize = optimize,
    });
    exe22.addIncludePath(.{.path = "lib"});
    exe22.addSystemIncludePath(.{.path = "."});
    exe22.addCSourceFiles(.{
        .files = &.{
            "src/newusers.c",
        },
        .flags = cflags,
    });
    exe22.linkLibC();
    exe22.linkLibrary(lib);
    b.installArtifact(exe22);

    const exe23 = b.addExecutable(.{
        .name = "pwck",
        .target = target,
        .optimize = optimize,
    });
    exe23.addIncludePath(.{.path = "lib"});
    exe23.addSystemIncludePath(.{.path = "."});
    exe23.addCSourceFiles(.{
        .files = &.{
            "src/pwck.c",
        },
        .flags = cflags,
    });
    exe23.linkLibC();
    exe23.linkLibrary(lib);
    b.installArtifact(exe23);

    const exe24 = b.addExecutable(.{
        .name = "pwconv",
        .target = target,
        .optimize = optimize,
    });
    exe24.addIncludePath(.{.path = "lib"});
    exe24.addSystemIncludePath(.{.path = "."});
    exe24.addCSourceFiles(.{
        .files = &.{
            "src/pwconv.c",
        },
        .flags = cflags,
    });
    exe24.linkLibC();
    exe24.linkLibrary(lib);
    b.installArtifact(exe24);
    
    const exe25 = b.addExecutable(.{
        .name = "pwunconv",
        .target = target,
        .optimize = optimize,
    });
    exe25.addIncludePath(.{.path = "lib"});
    exe25.addSystemIncludePath(.{.path = "."});
    exe25.addCSourceFiles(.{
        .files = &.{
            "src/pwunconv.c",
        },
        .flags = cflags,
    });
    exe25.linkLibC();
    exe25.linkLibrary(lib);
    b.installArtifact(exe25);

    const exe26 = b.addExecutable(.{
        .name = "useradd",
        .target = target,
        .optimize = optimize,
    });
    exe26.addIncludePath(.{.path = "lib"});
    exe26.addSystemIncludePath(.{.path = "."});
    exe26.addCSourceFiles(.{
        .files = &.{
            "src/useradd.c",
        },
        .flags = cflags,
    });
    exe26.linkLibC();
    exe26.linkLibrary(lib);
    b.installArtifact(exe26);

    const exe27 = b.addExecutable(.{
        .name = "userdel",
        .target = target,
        .optimize = optimize,
    });
    exe27.addIncludePath(.{.path = "lib"});
    exe27.addSystemIncludePath(.{.path = "."});
    exe27.addCSourceFiles(.{
        .files = &.{
            "src/userdel.c",
        },
        .flags = cflags,
    });
    exe27.linkLibC();
    exe27.linkLibrary(lib);
    b.installArtifact(exe27);
 
    const exe28 = b.addExecutable(.{
        .name = "usermod",
        .target = target,
        .optimize = optimize,
    });
    exe28.addIncludePath(.{.path = "lib"});
    exe28.addSystemIncludePath(.{.path = "."});
    exe28.addCSourceFiles(.{
        .files = &.{
            "src/usermod.c",
        },
        .flags = cflags,
    });
    exe28.linkLibC();
    exe28.linkLibrary(lib);
    b.installArtifact(exe28);
    
    const exe29 = b.addExecutable(.{
        .name = "vipw",
        .target = target,
        .optimize = optimize,
    });
    exe29.addIncludePath(.{.path = "lib"});
    exe29.addSystemIncludePath(.{.path = "."});
    exe29.addCSourceFiles(.{
        .files = &.{
            "src/vipw.c",
        },
        .flags = cflags,
    });
    exe29.linkLibC();
    exe29.linkLibrary(lib);
    b.installArtifact(exe29); 
}
