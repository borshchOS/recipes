#!/bin/sh -a

# Build cache is referenced by $1 and defaults to $HOME/.cache/kiss/proc/<num>/pkg/<name>
DESTDIR=$1
PREFIX=pkg/$(basename $1) # mimicOS packages install prefix
TARGET=${TARGET:-x86_64-linux-musl} # Zig build target
MCPU=${MCPU:-baseline} # Examples: `baseline`, `native`, `generic+v7a`, or `arm1176jzf_s`
CC="zig cc -fno-sanitize=all -s -target $TARGET -mcpu=$MCPU"
CXX="zig c++ -fno-sanitize=all -s -target $TARGET -mcpu=$MCPU"
CFLAGS="-fPIC -std=c11"
CXXLAGS="-fPIC -std=c++17"

cd source
./configure \
    --host=$TARGET \
    --build=$TARGET \
    --prefix=/$PREFIX \
    --sbindir=/$PREFIX/bin \
    --libdir=/$PREFIX/lib \
    --runstatedir=/run \
    --enable-shared=no \
    --enable-static=yes \
    --enable-auto-cleanup=yes \
    --enable-icu-config \
    --with-library-bits=64 \
    --disable-tests \
    --disable-samples \
    --disable-dyload \
    --with-data-packaging=static \
    > /dev/null 2>&1

# --with-data-packaging     specify how to package ICU data. Possible values:
#   files    raw files (.res, etc)
#   archive  build a single icudtXX.dat file
#   library  shared library (.dll/.so/etc.)
#   static   static library (.a/.lib/etc.)
#   auto     build shared if possible (default)
# See https://unicode-org.github.io/icu/userguide/icu_data for more info.

gmake -j$(nproc) > /dev/null 2>&1
gmake install > /dev/null 2>&1

ver="75.1"

# Generate .pc file
name="icu-uc"
version="$ver"
description="International Components for Unicode: Common and Data libraries."
url="https://icu.unicode.org/"
license="ICU"
requires="libc++"
libs="-L\${pcfiledir}/../../pkg/$(basename $1)/lib -licuuc -licudata"
cflags="-I\${pcfiledir}/../../pkg/$(basename $1)/include"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires
Libs: $libs
Cflags: $cflags
ICUDATA_NAME = icudt75l
ICUDESC=International Components for Unicode
UNICODE_VERSION=15.1
ICUPREFIX=icu
ICULIBSUFFIX=
LIBICU=libicu
pkglibdir=/pkg/icu/lib/icu/$ver
ICUDATA_DIR=/pkg/icu/share/icu/$ver
ICUPKGDATA_DIR=/pkg/icu/lib" > $name.pc

name="icu-i18n"
version="$ver"
description="International Components for Unicode: Internationalization library."
url="https://icu.unicode.org/"
license="ICU"
requires="icu-uc"
libs="-L\${pcfiledir}/../../pkg/$(basename $1)/lib -licui18n"
cflags="-I\${pcfiledir}/../../pkg/$(basename $1)/include"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires
Libs: $libs
Cflags: $cflags
ICUDATA_NAME = icudt75l
ICUDESC=International Components for Unicode
UNICODE_VERSION=15.1
ICUPREFIX=icu
ICULIBSUFFIX=
LIBICU=libicu
pkglibdir=/pkg/icu/lib/icu/$ver
ICUDATA_DIR=/pkg/icu/share/icu/$ver
ICUPKGDATA_DIR=/pkg/icu/lib" > $name.pc

name="icu-io"
version="$ver"
description="International Components for Unicode: Stream and I/O Library."
url="https://icu.unicode.org/"
license="ICU"
requires="icu-i18n"
libs="-L\${pcfiledir}/../../pkg/$(basename $1)/lib -licuio"
cflags="-I\${pcfiledir}/../../pkg/$(basename $1)/include"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires
Libs: $libs
Cflags: $cflags
ICUDATA_NAME = icudt75l
ICUDESC=International Components for Unicode
UNICODE_VERSION=15.1
ICUPREFIX=icu
ICULIBSUFFIX=
LIBICU=libicu
pkglibdir=/pkg/icu/lib/icu/$ver
ICUDATA_DIR=/pkg/icu/share/icu/$ver
ICUPKGDATA_DIR=/pkg/icu/lib" > $name.pc


# Install and link build artifacts
install -d -m 755 \
    $1/$PREFIX/share/pkgconfig \
    $1/bin \
    $1/share/man/man1 \
    $1/share/man/man8 \
    $1/share/pkgconfig

mv *.pc $1/$PREFIX/share/pkgconfig/

rm -rf $1/$PREFIX/lib/pkgconfig

for f in $(find $1/$PREFIX/bin/* -type fl); do
    ln -sf ../$PREFIX/bin/$(basename $f) $1/bin/$(basename $f); done

for f in $(find $1/$PREFIX/share/man/man1/* -type fl); do
    ln -sf ../../../$PREFIX/share/man/man1/$(basename $f) $1/share/man/man1/$(basename $f); done

for f in $(find $1/$PREFIX/share/man/man8/* -type fl); do
    ln -sf ../../../$PREFIX/share/man/man8/$(basename $f) $1/share/man/man8/$(basename $f); done
    
for f in $(find $1/$PREFIX/share/pkgconfig/* -type fl); do
    ln -sf ../../$PREFIX/share/pkgconfig/$(basename $f) $1/share/pkgconfig/$(basename $f); done
