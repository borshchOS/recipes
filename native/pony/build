#!/bin/sh -a

# Build cache is referenced by $1 and defaults to $HOME/.cache/kiss/proc/<num>/pkg/<name>
DESTDIR=$1
PREFIX=pkg/$(basename $1) # mimicOS packages install prefix
CMAKE_C_COMPILER=clang
CC=clang
CXX=clang++
CMAKE_ASM_COMPILER=${CMAKE_C_COMPILER}
ver="0.58.3"

patch lib/CMakeLists.txt < noextlibs.patch > /dev/null 2>&1
patch CMakeLists.txt < findlibs.patch > /dev/null 2>&1

cmake -S lib -B build/libs/lib \
  -Wno-dev \
  -DCMAKE_WARN_DEPRECATED=False \
  -DCMAKE_GENERATOR=Ninja \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_C_COMPILER=$CMAKE_C_COMPILER \
  \
  -DPONY_PIC_FLAG="-fPIC" \
  > /dev/null 2>&1

cmake --build build/libs/lib > /dev/null 2>&1

#sed -i.orig 's:CMAKE_CXX_STANDARD 14:CMAKE_CXX_STANDARD 17:g' CMakeLists.txt

# use libc++ and compiler-rt
sed -i.orig 's:${PROJECT_SOURCE_DIR}/../../build/libs/ -name:$KISS_ROOT/pkg/llvm15/lib -name:g' src/libponyc/CMakeLists.txt
sed -i.orig 's:libstdc++:libc++:g' src/libponyc/CMakeLists.txt
sed -i.orig 's:libstdcpp:libcpp:g' src/libponyc/CMakeLists.txt
sed -i.orig 's:-static-libstdc++:-static-libc++:g' src/libponyc/CMakeLists.txt
sed -i.orig 's:atomic:clang_rt.builtins-x86_64:g' src/ponyc/CMakeLists.txt
sed -i.orig 's:atomic:clang_rt.builtins-x86_64:g' test/libponyc/CMakeLists.txt
sed -i.orig 's:atomic:clang_rt.builtins-x86_64:g' benchmark/libponyc/CMakeLists.txt
sed -i.orig 's:atomic:clang_rt.builtins-x86_64:g' test/libponyrt/CMakeLists.txt
sed -i.orig 's:atomic:clang_rt.builtins-x86_64:g' benchmark/libponyrt/CMakeLists.txt

# Fix lib search and path
sed -i.orig 's:-latomic::g' src/libponyc/codegen/genexe.c
sed -i.orig 's:/usr/local/lib:/pkg/pony/lib:g' src/libponyc/pkg/package.c

# pick up AMDGPU target libs
sed -i.orig 's:"ARM":"AMDGPU":g' CMakeLists.txt
sed -i.orig 's:armasmparser:amdgpuasmparser:g' CMakeLists.txt
sed -i.orig 's:armcodegen:amdgpucodegen:g' CMakeLists.txt
sed -i.orig 's:armdesc:amdgpudesc:g' CMakeLists.txt
sed -i.orig 's:arminfo:amdgpuinfo:g' CMakeLists.txt

# use LLD
sed -i.orig 's:gold:lld:g' src/libponyc/codegen/genexe.c
sed -i.orig 's:gold:lld:g' src/libponyc/options/options.c

# musl does not need 64-bit function definitions
sed -i.orig 's:lseek64:lseek:g' packages/files/file.pony
sed -i.orig 's:ftruncate64:ftruncate:g' packages/files/file.pony

cmake -B build_pony \
  -Wno-dev \
  -DCMAKE_VERBOSE_MAKEFILE=OFF \
  -DCMAKE_WARN_DEPRECATED=False \
  -DCMAKE_GENERATOR=Ninja \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX=/pkg/pony \
  -DCMAKE_C_COMPILER=$CMAKE_C_COMPILER \
  -DCMAKE_C_FLAGS="${CMAKE_C_FLAGS} -I$KISS_ROOT/pkg/llvm15/include -fPIC -D_LARGEFILE64_SOURCE" \
  -DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} -I$KISS_ROOT/pkg/llvm15/include -stdlib=libc++ -fPIC" \
  -DCMAKE_EXE_LINKER_FLAGS="${CMAKE_EXE_LINKER_FLAGS} -L$KISS_ROOT/pkg/llvm-project/lib/linux" \
  \
  -DPONY_ARCH=native \
  -DPONY_PIC_FLAG="-fPIC" \
  -DPONYC_VERSION=$ver \
  -DPONY_USE_LTO=true \
  -DPONY_RUNTIME_BITCODE=false \
  -DPONY_USE_VALGRIND=false \
  -DPONY_USE_THREAD_SANITIZER=false \
  -DPONY_USE_ADDRESS_SANITIZER=false \
  -DPONY_USE_UNDEFINED_BEHAVIOR_SANITIZER=false \
  -DPONY_USE_COVERAGE=false \
  -DPONY_USE_POOLTRACK=false \
  -DPONY_USE_DTRACE=false \
  -DPONY_USE_SCHEDULER_SCALING_PTHREADS=false \
  -DPONY_USE_SYSTEMATIC_TESTING=false \
  -DPONY_USE_RUNTIMESTATS=false \
  -DPONY_USE_RUNTIMESTATS_MESSAGES=false \
  -DPONY_USE_POOL_MEMALIGN=false \
  \
  -Dzstd_INCLUDE_DIR="$KISS_ROOT/pkg/zstd/include" \
  -Dzstd_LIBRARY="$KISS_ROOT/pkg/zstd/lib/libzstd.a" \
  -DTerminfo_LIBRARIES="$KISS_ROOT/pkg/ncurses/lib/libncurses.a" \
  -DTerminfo_LINKABLE=yes \
  > /dev/null 2>&1

cmake --build   build_pony > /dev/null 2>&1
cmake --install build_pony > /dev/null 2>&1

# Generate .pc file
name="pony"
version="$ver"
description="Pony programming language compiler"
url="https://www.ponylang.io/"
license="BSD-2-Clause"
requires=""
libs="-L\${pcfiledir}/../../pkg/$(basename $1)/lib -lponyc -lponyrt"
cflags=""

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires
Libs: $libs
Cflags: $cflags" > $name.pc

# Install and link build artifacts
install -d -m 755 \
    $1/$PREFIX/share/pkgconfig \
    $1/bin \
    $1/share/pkgconfig

mv *.pc $1/$PREFIX/share/pkgconfig/

ln -sf ../$PREFIX/bin/ponyc $1/bin/ponyc

install -m 644 release/libponyc.a $1/$PREFIX/lib
install -m 644 release/libponyc-standalone.a $1/$PREFIX/lib
install -m 644 release/libponyrt-pic.a $1/$PREFIX/lib

for f in $(find $1/$PREFIX/share/pkgconfig/* -type fl); do
    ln -sf ../../$PREFIX/share/pkgconfig/$(basename $f) $1/share/pkgconfig/$(basename $f); done
