#!/bin/sh

if [ ! -e "$XDG_RUNTIME_DIR/ssh-agent" ]; then
    # start new ssh-agent
    eval $(ssh-agent -s) >/dev/null 2>&1
    echo "${SSH_AUTH_SOCK}" > "$XDG_RUNTIME_DIR/ssh-agent"
    echo "${SSH_AGENT_PID}" >> "$XDG_RUNTIME_DIR/ssh-agent"
else
    # reuse existing agent's vars for the same user
    export SSH_AUTH_SOCK="$(head -n 1 $XDG_RUNTIME_DIR/ssh-agent)"
    export SSH_AGENT_PID="$(tail -1 $XDG_RUNTIME_DIR/ssh-agent)"
fi
