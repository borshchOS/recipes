#!/bin/sh -a

# Build cache is referenced by $1 and defaults to $HOME/.cache/kiss/proc/<num>/pkg/<name>
DESTDIR=$1
PREFIX=pkg/$(basename $1) # mimicOS packages install prefix
TARGET=${TARGET:-x86_64-linux-musl} # Zig build target
MCPU=${MCPU:-baseline} # Examples: `baseline`, `native`, `generic+v7a`, or `arm1176jzf_s`
CC=clang
PATH=$KISS_ROOT/bin
PKG_CONFIG_PATH=$KISS_ROOT/share/pkgconfig

# fix harfbuzz include
sed -i.orig 's:<harfbuzz/:<:g' fcft.c

python -m mesonbuild.mesonmain setup build/ \
    --prefix=/$PREFIX \
    --libdir=lib \
    --sysconfdir=/etc \
    -Dbuildtype=minsize \
    -Ddefault_library=static \
    -Dprefer_static=true \
    -Db_staticpic=true \
    -Db_pie=false \
    \
    -D docs=disabled \
    -D svg-backend=nanosvg \
    -D grapheme-shaping=enabled \
    -D run-shaping=enabled \
    > /dev/null 2>&1
    
ninja -C build/ install > /dev/null 2>&1

# Generate .pc file
name="fcft"
version="3.1.8"
description="simple font loading and glyph rasterization library"
url="https://codeberg.org/dnkl/fcft"
license="MIT"
requires="fontconfig, freetype2, harfbuzz, libutf8proc, pixman-1, tllist >= 1.0.1"
libs="-L\${pcfiledir}/../../pkg/$(basename $1)/lib -lfcft"
cflags="-I\${pcfiledir}/../../pkg/$(basename $1)/include"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires
Libs: $libs
Cflags: $cflags" > $name.pc

# Install and link build artifacts
install -d -m 755 \
    $1/$PREFIX/share/pkgconfig \
    $1/share/pkgconfig

cp -a *.pc $1/$PREFIX/share/pkgconfig/

rm -rf $1/$PREFIX/lib/pkgconfig

for f in $(find $1/$PREFIX/share/pkgconfig/* -type fl); do
    ln -sf ../../$PREFIX/share/pkgconfig/$(basename $f) $1/share/pkgconfig/$(basename $f); done
