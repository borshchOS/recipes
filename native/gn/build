#!/idx/bin/sh -a

TARGET=${TARGET:-x86_64-linux-musl} # Zig build target
MCPU=${MCPU:-baseline} # Examples: `baseline`, `native`, `generic+v7a`, or `arm1176jzf_s`
PREFIX=${PREFIX:-pkg/$(basename ${1})} # Zig build artifact install prefix
KISS_ROOT=${KISS_ROOT:-/} # Package install prefix
DESTDIR=${DESTDIR:-${1}} # Build cache. ${1} defaults to $HOME/.cache/kiss/proc/<num>/pkg/<name>
CC="zig cc -fno-sanitize=all -s -target ${TARGET} -mcpu=${MCPU}"
CXX="zig c++ -fno-sanitize=all -s -target ${TARGET} -mcpu=${MCPU}"
CFLAGS="-fPIC"
CXXFLAGS="-fPIC"
CMAKE_C_COMPILER="zig;cc;-fno-sanitize=all;-s;-target;${TARGET};-mcpu=${MCPU}"
CMAKE_CXX_COMPILER="zig;c++;-fno-sanitize=all;-s;-target;${TARGET};-mcpu=${MCPU}"
CMAKE_ASM_COMPILER=${CMAKE_C_COMPILER}
PKG_CONFIG_PATH="${KISS_ROOT}/idx/pcf"

OPTIMIZE=${OPTIMIZE:-ReleaseSmall} # Zig build options: Debug, ReleaseSafe, ReleaseFast, ReleaseSmall

# Generate .pc file
name="gn"
description="GN is a meta-build system that generates build files for Ninja."
url="https://gn.googlesource.com/gn/"
license="BSD-3-Clause"
version="e4702d7409069c4f12d45ea7b7f0890717ca3f4b"
requires=""

echo "Name: ${name}
Description: ${description}
URL: ${url}
License: ${license}
Version: ${version}
Requires: ${requires}" > ${name}.pc

echo '#ifndef OUT_LAST_COMMIT_POSITION_H_
#define OUT_LAST_COMMIT_POSITION_H_

#define LAST_COMMIT_POSITION_NUM 2124
#define LAST_COMMIT_POSITION "2124 (e4702d740906)"

#endif' > gn/last_commit_position.h

zig build \
    -Dtarget=${TARGET} \
    -Doptimize=${OPTIMIZE} \
    --search-prefix ${KISS_ROOT} \
    --prefix ${PREFIX}

# Install and link build artifacts
install -d -m 755 \
    ${DESTDIR}/${PREFIX}/pcf \
    ${DESTDIR}/idx/bin \
    ${DESTDIR}/idx/pcf

cp -a ${name}.pc ${DESTDIR}/${PREFIX}/pcf/

for f in $(find ${DESTDIR}/${PREFIX}/bin/* -type fl); do
    ln -sf ../../${PREFIX}/bin/$(basename ${f}) ${DESTDIR}/idx/bin/$(basename ${f});done

for f in $(find ${DESTDIR}/${PREFIX}/pcf/* -type fl); do
    ln -sf ../../${PREFIX}/pcf/$(basename ${f}) ${DESTDIR}/idx/pcf/$(basename ${f});done
