const std = @import("std");

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const cflags = &[_][]const u8{
        "-pipe",
        "-DANOTHER_BRICK_IN_THE",
        "-fno-common",
        "-fdiagnostics-show-option",
        "-fvisibility=hidden",
        "-ffunction-sections",
        "-fdata-sections",
        "-D_GNU_SOURCE",
        "-DHAVE_SECURE_GETENV",
        "-DHAVE_DECL_BE32TOH",
        "-DHAVE_LINUX_MODULE_H",
        "-DHAVE___BUILTIN_UADDL_OVERFLOW",
        "-DHAVE___BUILTIN_UADDLL_OVERFLOW",
        "-DENABLE_EXPERIMENTAL",
        "-DHAVE_DECL_STRNDUPA",
        
        "-DPACKAGE=\"kmod\"",
        "-DKMOD_FEATURES=\"+libzstd +liblzma +libz +libcrypto +experimental\"",
        "-DVERSION=\"32\"", //NOTE: don't forget to update when bumping up version
        
        "-DENABLE_OPENSSL",
        "-DENABLE_ZSTD",
        "-DENABLE_XZ",
        "-DENABLE_ZLIB",
        
        "-DSYSCONFDIR=\"/etc\"",
        "-DDISTCONFDIR=\"/etc\"",
        "-DMODULE_DIRECTORY=\"/pkg/linux/lib/modules\"",
    };

    const lib = b.addStaticLibrary(.{
        .name = "kmod",
        .target = target,
        .optimize = optimize,
        .pic = true,
    });
    lib.linkLibC();
    lib.addSystemIncludePath(b.path("shared"));
    lib.addSystemIncludePath(b.path("."));
    lib.addCSourceFiles(.{
        .files = &.{
            "shared/array.c",
	        "shared/hash.c",
	        "shared/scratchbuf.c",
	        "shared/strbuf.c",
	        "shared/util.c",
	        "libkmod/libkmod.c",
	        "libkmod/libkmod-builtin.c",
	        "libkmod/libkmod-list.c",
	        "libkmod/libkmod-config.c",
	        "libkmod/libkmod-index.c",
	        "libkmod/libkmod-module.c",
	        "libkmod/libkmod-file.c",
	        "libkmod/libkmod-elf.c",
	        "libkmod/libkmod-signature.c",
        },
        .flags = cflags,
    });

    const lib1 = b.addSharedLibrary(.{
        .name = "kmod",
        .target = target,
        .optimize = optimize,
        .pic = true,
    });
    lib1.linkLibC();
    lib1.addSystemIncludePath(b.path("shared"));
    lib1.addSystemIncludePath(b.path("."));
    lib1.addCSourceFiles(.{
        .files = &.{
            "shared/array.c",
            "shared/hash.c",
            "shared/scratchbuf.c",
            "shared/strbuf.c",
            "shared/util.c",
            "libkmod/libkmod.c",
            "libkmod/libkmod-builtin.c",
            "libkmod/libkmod-list.c",
            "libkmod/libkmod-config.c",
            "libkmod/libkmod-index.c",
            "libkmod/libkmod-module.c",
            "libkmod/libkmod-file.c",
            "libkmod/libkmod-elf.c",
            "libkmod/libkmod-signature.c",
        },
        .flags = cflags,
    });

    const exe = b.addExecutable(.{
        .name = "kmod",
        .target = target,
        .optimize = optimize,
    });
    exe.addSystemIncludePath(b.path("shared"));
    exe.addSystemIncludePath(b.path("."));
    exe.linkLibC();
    exe.linkLibrary(lib);
    exe.linkSystemLibrary2("zstd", .{ .preferred_link_mode = .static });
    exe.linkSystemLibrary2("z", .{ .preferred_link_mode = .static });
    exe.linkSystemLibrary2("lzma", .{ .preferred_link_mode = .static });
    exe.linkSystemLibrary2("crypto", .{ .preferred_link_mode = .static });
    exe.addCSourceFiles(.{
        .files = &.{
            "tools/kmod.c",
	        "tools/lsmod.c",
	        "tools/rmmod.c",
	        "tools/insmod.c",
	        "tools/modinfo.c",
	        "tools/modprobe.c",
	        "tools/depmod.c",
	        "tools/log.c",
	        "tools/static-nodes.c",
        },
        .flags = cflags,
    });
    
    b.installArtifact(lib);
    b.installArtifact(lib1);
    b.installArtifact(exe);
}
