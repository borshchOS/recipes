const std = @import("std");

pub fn build(b: *std.Build) !void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const exe = b.addExecutable(.{
        .name = "janet",
        .target = target,
        .optimize = optimize,
    });
    exe.addCSourceFiles(.{
        .dependency = null,
        .files = &.{
            "shell.c",
        },
        .flags = &.{
            "-std=c99",
            "-fvisibility=hidden",
            "-Wall",
            "-Wextra",
        },
    });
    exe.linkLibC();
    exe.linkSystemLibrary("janet");
    b.installArtifact(exe);

    b.installFile("janet.1", "share/man/man1/janet.1");
}
