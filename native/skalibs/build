#!/bin/sh -a

# Build cache is referenced by $1 and defaults to $HOME/.cache/kiss/proc/<num>/pkg/<name>
DESTDIR=$1
PREFIX=pkg/$(basename $1) # mimicOS packages install prefix
TARGET=${TARGET:-x86_64-linux-musl} # Zig build target
MCPU=${MCPU:-baseline} # Examples: `baseline`, `native`, `generic+v7a`, or `arm1176jzf_s`
CC="zig cc -fno-sanitize=all -s -target $TARGET -mcpu=$MCPU"
CFLAGS="-fPIC"
LD=$CC

./configure \
    --host=$TARGET \
    --build=$TARGET \
    --prefix=/$PREFIX \
    --sbindir=/$PREFIX/bin \
    --runstatedir=/run \
    --disable-shared \
    --with-sysdep-devurandom=yes \
    --with-sysdep-arc4random=no \
    --with-sysdep-arc4random_addrandom=no \
    --with-sysdep-getpeereid=no \
    --with-sysdep-sopeercred=yes \
    --with-default-path=/bin \
    > /dev/null 2>&1
    
gmake -j$(nproc) > /dev/null 2>&1
gmake install > /dev/null 2>&1

# Generate .pc file
name="skalibs"
version="2.14.1.1"
description="Set of general-purpose C programming libraries for skarnet.org software"
url="https://skarnet.org/software/skalibs"
license="ISC"
requires=""
libs="-L\${pcfiledir}/../../pkg/$(basename $1)/lib/$name -lskarnet"
cflags="-I\${pcfiledir}/../../pkg/$(basename $1)/include"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires
Libs: $libs
Cflags: $cflags" > $name.pc

# Install and link build artifacts
install -d -m 755 \
    $1/$PREFIX/share/pkgconfig \
    $1/share/pkgconfig

mv $name.pc $1/$PREFIX/share/pkgconfig/

for f in $(find $1/$PREFIX/share/pkgconfig/* -type fl); do
    ln -sf ../../$PREFIX/share/pkgconfig/$(basename $f) $1/share/pkgconfig/$(basename $f); done
