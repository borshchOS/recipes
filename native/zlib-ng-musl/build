#!/bin/sh -a

# Build cache is referenced by $1 and defaults to $HOME/.cache/kiss/proc/<num>/pkg/<name>
DESTDIR=$1
PREFIX=pkg/$(basename $1) # mimicOS packages install prefix

# Use this to define pkg names and dirs, when built with different libc (e.g. "-musl", "-gnu")
SUFFIX="-musl"
PKG_CONFIG_PATH=$KISS_ROOT/lib/pkgconfig$SUFFIX

TARGET=${TARGET:-x86_64-linux-musl} # Zig build target
MCPU=${MCPU:-baseline} # Examples: `baseline`, `native`, `generic+v7a`, or `arm1176jzf_s`

CC="clang"
CFLAGS+="-fPIC "
CFLAGS+="-I$KISS_ROOT/pkg/musl/include "
LDFLAGS+="-L$KISS_ROOT/pkg/llvm-toolchain/lib "
LDFLAGS+="-L$KISS_ROOT/pkg/musl/lib "

# Build zlib in-place
cmake -B build-zlib \
  -Wno-dev \
  -DCMAKE_WARN_DEPRECATED=False \
  -DCMAKE_GENERATOR=Ninja \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX=/pkg/$(basename $1) \
  -DCMAKE_INSTALL_LIBDIR="lib" \
  -DCMAKE_SKIP_RPATH=YES \
  \
  -DZLIB_COMPAT=ON \
  -DZLIB_ENABLE_TESTS=OFF \
  -DWITH_NATIVE_INSTRUCTIONS=ON \
  -DWITH_GTEST=OFF \
  > /dev/null 2>&1

cmake --build   build-zlib > /dev/null 2>&1
cmake --install build-zlib > /dev/null 2>&1

# Generate .pc file
name="zlib"
version="1.3.0.zlib-ng"
description="zlib replacement with optimizations for next generation systems."
url="https://github.com/zlib-ng/zlib-ng"
license="Zlib"
requires=""
reqpriv=""
libs="-L/pkg/$(basename $1)/lib -lz"
libspriv="-pthread"
cflags="-I/pkg/$(basename $1)/include -DWITH_GZFILEOP"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires
Requires.private: $reqpriv
Libs: $libs
Libs.private: $libspriv
Cflags: $cflags" > $name.pc

# Install and link build artifacts
install -d -m 755 \
    $1/$PREFIX/lib/pkgconfig$SUFFIX \
    $1/lib/pkgconfig$SUFFIX

mv $name.pc $1/$PREFIX/lib/pkgconfig$SUFFIX/
rm -rf $1/$PREFIX/lib/pkgconfig

for f in $(find $1/$PREFIX/lib/pkgconfig$SUFFIX/* -type f && find $1/$PREFIX/lib/pkgconfig$SUFFIX/* -type l); do
    ln -sf ../../$PREFIX/lib/pkgconfig$SUFFIX/$(basename $f) $1/lib/pkgconfig$SUFFIX/$(basename $f); done


