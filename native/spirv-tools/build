#!/bin/sh -a

# Build cache is referenced by $1 and defaults to $HOME/.cache/kiss/proc/<num>/pkg/<name>
DESTDIR=$1
PREFIX=pkg/$(basename $1) # mimicOS packages install prefix
TARGET=${TARGET:-x86_64-linux-musl} # Zig build target
MCPU=${MCPU:-baseline} # Examples: `baseline`, `native`, `generic+v7a`, or `arm1176jzf_s`
CC="zig cc -fno-sanitize=all -s -target $TARGET -mcpu=$MCPU"
CXX="zig c++ -fno-sanitize=all -s -target ${TARGET} -mcpu=${MCPU}"
CMAKE_C_COMPILER="zig;cc;-fno-sanitize=all;-s;-target;${TARGET};-mcpu=${MCPU}"
CMAKE_CXX_COMPILER="zig;c++;-fno-sanitize=all;-s;-target;${TARGET};-mcpu=${MCPU}"
CMAKE_ASM_COMPILER=${CMAKE_C_COMPILER}
CFLAGS="-fPIC"
CXXFLAGS="-fPIC"

patch source/CMakeLists.txt < spirv-tools-noshlib.patch > /dev/null 2>&1
sed -i.orig 's:\${SPIRV_TOOLS}-shared::g' source/CMakeLists.txt

cmake -B build \
  -Wno-dev \
  -DCMAKE_WARN_DEPRECATED=False \
  -DCMAKE_GENERATOR=Ninja \
  -DCMAKE_BUILD_TYPE=MinSizeRel \
  -DCMAKE_INSTALL_PREFIX=/pkg/spirv-tools \
  -DCMAKE_C_COMPILER=$CMAKE_C_COMPILER \
  -DCMAKE_CXX_COMPILER=$CMAKE_CXX_COMPILER \
  -DCMAKE_INSTALL_LIBDIR="lib" \
  -DCMAKE_SKIP_RPATH=YES \
  -DBUILD_SHARED_LIBS=OFF \
  -DSPIRV-Headers_SOURCE_DIR="${KISS_ROOT}/pkg/spirv-headers" \
  -DSPIRV_SKIP_TESTS=ON \
  -DSPIRV_TOOLS_BUILD_STATIC=ON \
  -DSPIRV_TOOLS_FULL_VISIBILITY=ON \
  -DENABLE_SPIRV_TOOLS_INSTALL=ON \
  > /dev/null 2>&1 

cmake --build   build > /dev/null 2>&1
cmake --install build > /dev/null 2>&1

ver="2024.1"

# Generate .pc file
name="SPIRV-Tools"
version="$ver"
description="Tools for SPIR-V"
url="https://github.com/KhronosGroup/SPIRV-Tools"
license="Apache-2.0"
requires=""
libs="-L\${pcfiledir}/../../pkg/$(basename $1)/lib -lSPIRV-Tools-opt -lSPIRV-Tools -lSPIRV-Tools-link"
cflags="-I\${pcfiledir}/../../pkg/$(basename $1)/include"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires
Libs: $libs
Cflags: $cflags" > $name.pc

name="SPIRV-Tools-shared"
version="$ver"
description="Tools for SPIR-V"
url="https://github.com/KhronosGroup/SPIRV-Tools"
license="Apache-2.0"
requires=""
libs="-L\${pcfiledir}/../../pkg/$(basename $1)/lib -lSPIRV-Tools-shared"
cflags="-I\${pcfiledir}/../../pkg/$(basename $1)/include"

echo "Name: $name
Version: $version
Description: $description
URL: $url
License: $license
Requires: $requires
Libs: $libs
Cflags: $cflags" > $name.pc

# Install and link build artifacts
install -d -m 755 \
    $1/$PREFIX/share/pkgconfig \
    $1/bin \
    $1/share/pkgconfig

cp -a *.pc $1/$PREFIX/share/pkgconfig/

rm -rf $1/$PREFIX/lib/pkgconfig 

for f in $(find $1/$PREFIX/bin/* -type fl); do
    ln -sf ../$PREFIX/bin/$(basename $f) $1/bin/$(basename $f); done

for f in $(find $1/$PREFIX/share/pkgconfig/* -type fl); do
    ln -sf ../../$PREFIX/share/pkgconfig/$(basename $f) $1/share/pkgconfig/$(basename $f); done
